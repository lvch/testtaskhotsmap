## Тестовое задание
Клиентской код расположен в app/Controller/UserController.php

### Пример работы
```php
    
    $client = new \App\GuzzleClient();
    $auth = new \App\Auth($client);

    $user = new \App\Controller\UserController($auth, $client);
    $user->getUserDataRequest();

    // example for update user data
    $updateData = [
        "active" => "1",
        "blocked" => true,
        "name" => "Petr Petrovich",
        "permissions" => [
            [
                "id" => 1,
                "permission" => "comment",
            ]
        ],
    ];

    $user->setUserDataRequest($updateData);
```
