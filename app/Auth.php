<?php

namespace App;

use App\Environment;
use App\GuzzleClient;
use App\Helper\Log;

class Auth
{
    private static $token = '';
    private static $tokenTime = 0;
    private $client;

    public function __construct(GuzzleClient $client) {
        $this->client = $client;
    }

    /**
     * Get authorization token
     * @return string
     */
    public function getToken() : string {
        if (
            empty(self::$token)
            || (self::$tokenTime - time() > 60 * 5)
        ) {
            $this->requestToken();
        }

        return self::$token;
    }

    private function requestToken() : void {
        $result = $this->client->makeRequest('GET','http://testapi.ru/auth',[
            'auth' => [Environment::LOGIN, Environment::PASSWORD]
        ]);

        $reqResult = [];

        if ($result === "error") {
            $reqResult["error"] = GuzzleClient::REQUEST_ERROR;
            Log::logError(self::class . " " . GuzzleClient::REQUEST_ERROR);
        } else {
            if ($result !== '') {
                $reqResult = json_decode($result, true);

                if (array_key_exists("status", $reqResult)) {
                    switch ($reqResult["status"]) {
                        case ApiStatuses::ERROR:
                        {
                            $reqResult["error"] = "Status: ". ApiStatuses::ERROR;
                            Log::logError(self::class . " " . "Status: ". ApiStatuses::ERROR);
                            break;
                        }

                        case ApiStatuses::NOT_FOUND:
                        {
                            $reqResult["error"] = "Status: ". ApiStatuses::NOT_FOUND;
                            Log::logError(self::class . " " . "Status: ". ApiStatuses::NOT_FOUND);
                            break;
                        }

                        default:
                        {
                            $reqResult["error"] = "";
                            self::$token = $reqResult['token'];
                            self::$tokenTime = time();
                            break;
                        }
                    }
                }
            } else {
                $reqResult["error"] = GuzzleClient::EMPTY_REQUEST;
                Log::logError(self::class . " " . GuzzleClient::REQUEST_ERROR);
            }
        }
    }
}
