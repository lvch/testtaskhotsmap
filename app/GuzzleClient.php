<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class GuzzleClient
{

    const REQUEST_ERROR = "Error. Something happens while sending request.";
    const EMPTY_REQUEST = "Empty response body";

    /**
     * Send HTTP Request
     * @param $method
     * @param $uri
     * @param $options
     * @return string
     */
    public function makeRequest($method, $uri, $options) : string {
        $client = new Client();

        try {
            $res = $client->request($method, $uri, $options);

            if ($res->getStatusCode() >= 300) {
                $result = "error";
            } else {
                $result = (string) $res->getBody();
            }
        } catch (GuzzleException $exception) {
            $result = "error";
        }

        return $result;
    }

}
