<?php


namespace App\Controller;


use App\ApiStatuses;
use App\Auth;
use App\DataType\UserData;
use App\Environment;
use App\GuzzleClient;
use App\Helper\Log;

class UserController extends RequestController
{
    /** @var UserData */
    private $userData;

    /** @var Auth */
    private $auth;

    /** @var GuzzleClient */
    private $client;

    public function __construct(Auth $auth, GuzzleClient $client) {
        $this->auth = $auth;
        $this->client = $client;
    }

    public function getToken() :string {
        return $this->auth->getToken();
    }
    /**
     * Get user data from API
     */
    public function getUserDataRequest() {

        $username = Environment::USERNAME;
        $token = $this->getToken();
        $reqResult = [];

        $result = $this->client->makeRequest('GET',"http://testapi.ru/get-user/".$username."?token=".$token, []);

        if ($result === "error") {
            $reqResult["error"] = GuzzleClient::REQUEST_ERROR;
            Log::logError(self::class . " " . GuzzleClient::REQUEST_ERROR);
        } else {

            $reqResult = $this->setErrorStatus($result);

            if ($reqResult["error"] === "") {
                $this->userData = new UserData($reqResult);
            }
        }

        $this->presentData($reqResult);
    }

    /**
     * Set user data through API
     * @param array $updateData
     *  $updateData = [
            "active" => "1",
            "blocked" => true,
            "name" => "Petr Petrovich",
            "permissions" => [
                "id" => 1,
                "permission" => "comment",
            ],
        ];
     */
    public function setUserDataRequest(array $updateData) {

        $token = $this->getToken();
        $reqResult = [];

        //copy real data
        $tempUserData = clone $this->userData;

        $tempUserData->updateFields($updateData);
        $result = $this->client->makeRequest('POST',
            "http://testapi.ru/user/".$tempUserData->getId()."/update?token=".$token,
            $updateData
        );

        if ($result === "error") {
            $reqResult["error"] = GuzzleClient::REQUEST_ERROR;
            Log::logError(self::class . " " . GuzzleClient::REQUEST_ERROR);
        } else {
            $reqResult = $this->setErrorStatus($result);
            if ($reqResult["error"] !== "") {
                $this->userData = $tempUserData;
            } else {
                unset($tempUserData);
            }
        }

        $this->presentData($reqResult);
    }

    /**
     * @return UserData
     */
    public function getUserData(): UserData {
        return $this->userData;
    }

    private function setErrorStatus($result) {
        $reqResult = [];
        if ($result !== '') {
            $reqResult = json_decode($result,true);

            if (array_key_exists("status", $reqResult)) {
                switch ($reqResult["status"]) {
                    case ApiStatuses::ERROR:
                    {
                        $reqResult["error"] = "Status: ". ApiStatuses::ERROR;
                        Log::logError(self::class . " " . "Status: ". ApiStatuses::ERROR);
                        break;
                    }

                    case ApiStatuses::NOT_FOUND:
                    {
                        $reqResult["error"] = "Status: ". ApiStatuses::NOT_FOUND;
                        Log::logError(self::class . " " . "Status: ". ApiStatuses::NOT_FOUND);
                        break;
                    }

                    default: {
                        $reqResult["error"] = "";
                        break;
                    }
                }
            }
        } else {
            $reqResult["error"] = GuzzleClient::EMPTY_REQUEST;
            Log::logError(self::class . " " . GuzzleClient::EMPTY_REQUEST);
        }

        return $reqResult;

    }
}
