<?php


namespace App\DataType;


class UserData
{
    /** @var string **/
    private $status;

    /** @var string **/
    private $active;

    /** @var bool **/
    private $blocked;

    /** @var int **/
    private $created_at;

    /** @var int **/
    private $id;

    /** @var string **/
    private $name;

    /** @var Permissions[] **/
    private $permissions;

    public function __construct(array $data) {
        $this->permissions = [];
        $this->updateFields($data);
    }

    public function updateFields(array $data) {
        if (array_key_exists("status", $data)) {
            $this->status = $data["status"];
        }

        if (array_key_exists("active", $data)) {
            $this->active = $data["active"];
        }

        if (array_key_exists("blocked", $data)) {
            $this->blocked =  $data["blocked"];
        }

        if (array_key_exists("created_at", $data)) {
            $this->created_at = $data["created_at"];
        }

        if (array_key_exists("id", $data)) {
            $this->id = $data["id"];
        }

        if (array_key_exists("name", $data)) {
            $this->name = $data["name"];
        }

        if (array_key_exists("permissions", $data)) {
            $this->setPermissions($data["permissions"]);
        }
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getActive(): string
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function getBlocked(): bool
    {
        return $this->blocked;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Permissions[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param array $data
     */
    public function setPermissions(array $data): void
    {
        foreach ($data as $permission) {
            if (isset($this->permissions)) {
                $changed = false;
                foreach ($this->permissions as $localPermission) {
                    if ($localPermission->getId() === $permission["id"]) {
                        $localPermission->setPermission($permission["permission"]);
                        $changed = true;
                        break;
                    }
                }

                if (!$changed) {
                    array_push($this->permissions, new Permissions($permission));
                }
            } else {
                array_push($this->permissions, new Permissions($permission));
            }
        }
    }
}
