<?php


namespace App\DataType;


class Permissions
{
    /** @var int **/
    private $id;

    /** @var string **/
    private $permission;

    public function __construct(array $data) {
        if (array_key_exists("id", $data)) {
            $this->id = $data["id"];
        }

        if (array_key_exists("permission", $data)) {
            $this->permission = $data["permission"];
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPermission(): string
    {
        return $this->permission;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $permission
     */
    public function setPermission(string $permission): void
    {
        $this->permission = $permission;
    }
}
