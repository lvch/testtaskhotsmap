<?php


namespace App;


class ApiStatuses
{
    const ERROR = "Error";
    const NOT_FOUND = "Not found";
    const OK = "Ok";

}
