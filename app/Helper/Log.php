<?php

namespace App\Helper;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Log
{
    private static $logger;

    private static function getLogger(): Logger {
        if (!isset(self::$logger)) {
            self::$logger = new Logger('my_logger');
            self::$logger->pushHandler(new StreamHandler(__DIR__.'../../app.log', Logger::DEBUG));

        }
        return self::$logger;
    }

    public static function logError(string $text) {
        self::getLogger()->error($text);
    }
}
